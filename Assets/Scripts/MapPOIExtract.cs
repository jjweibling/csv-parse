using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Globalization;



public class MapPOIExtract : MonoBehaviour
{

    //Define input text file. Presume Unity connection
    public TextAsset csvInput;
    public List<MapPOIData> MapPOIRoot;

    void Start()
    {


        //Retrieve MapPOI object

        string tmpString = csvInput.text;

        string[] csvLines = tmpString.Split('\n');

        for (int i = 1; i < csvLines.Length; i++)
        {
            //Split lines into individual entries
            string[] csvEntries = csvLines[i].Split(',');
            //New Object
            MapPOIData data = new MapPOIData();

            Debug.Log(csvEntries[20] + csvEntries[20].Length + string.IsNullOrWhiteSpace(csvEntries[20]));
            //Location Information
            data.poiname = csvEntries[0];
            Debug.Log(" Name: " + data.poiname);


            if (!string.IsNullOrWhiteSpace(csvEntries[20]))
            {
                data.locationBeaconData.latitude = double.Parse(csvEntries[20], NumberStyles.AllowTrailingSign);
                Debug.Log(" Lat: " + data.locationBeaconData.latitude.ToString());
            }
            if (!string.IsNullOrWhiteSpace(csvEntries[21]))
            {
                data.locationBeaconData.longitude = double.Parse(csvEntries[21], NumberStyles.AllowTrailingSign);
                data.locationBeaconData.longitude.ToString();
            }

            // Check boolean values represented by unknown chars
            if (csvEntries[10] != "") { data.overlaymedia = true; }
            if (csvEntries[11] != "") { data.ambientmedia = true; }
            if (csvEntries[12] != "") { data.minigame = true; }
            if (csvEntries[13] != "") { data.coupon = true; }
            if (csvEntries[14] != "") { data.merch = true; }
            if (csvEntries[15] != "") { data.data = true; }


            //
            Debug.Log(" Overlay Media: " + data.overlaymedia);
            Debug.Log(" Ambient Media :" + data.ambientmedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
            Debug.Log("Overlay Media" + data.overlaymedia);
        }
    }
}
